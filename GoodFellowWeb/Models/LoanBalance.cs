﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoodFellowWeb.Models
{
    public class LoanBalance
    {
       public string Loan { get; set; } 
       public decimal Balance { get; set; }
    }
}