﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoodFellowWeb.Models
{
   public class LoanStatement
    {
       public DateTime TrxnDate { get; set; }
      public decimal Amount { get; set; }
      public string Refrence { get; set; }
     public string Description {get;set;}

        public decimal Balance { get; set; }
    }
}