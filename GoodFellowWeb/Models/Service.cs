﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodFellowWeb.Models
{
    public class Service
    {
        public string baseUrl = "http://192.168.3.245/CMSAppG";

        public List<LCounts> loanC(string s)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("LoanN/{t}", Method.GET);
            request.AddUrlSegment("t",s.Replace("*", "-"));
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<LCounts> dataList = JsonConvert.DeserializeObject<List<LCounts>>(validate);
            return dataList;
        }

        public dynamic loanCN(string s)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("nameN/{t}", Method.GET);
            request.AddUrlSegment("t", s.Replace("*", "-"));
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            return validate;
        }


    }
}