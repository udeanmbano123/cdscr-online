﻿
using GoodFellowWeb.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace GoodFellowWeb.DAO
{
    public class  SBoardContext : DbContext
    {
        public SBoardContext()
            : base("SBoardConnection")
        {}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>()
                .HasMany(u => u.Roles)
                .WithMany(r => r.Users)
                .Map(m =>
                {
                    m.ToTable("UserRoles");
                    m.MapLeftKey("UserId");
                    m.MapRightKey("RoleId");
                });
            //modelBuilder.Entity<TradingPlatform>()
            //      .HasRequired(e => e.TradingBoards)
            //      .WithMany(d => d.TradingPlatforms);
            base.OnModelCreating(modelBuilder);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

    }



}