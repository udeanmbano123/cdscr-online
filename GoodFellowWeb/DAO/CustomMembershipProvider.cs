﻿using GoodFellowWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace GoodFellowWeb.DAO.security
{
    public class CustomMembershipProvider
    {
        readonly SBoardContext Context = new SBoardContext();


        public bool CreateUser(string username, string password, string email)
        {

            try
            {
                User NewUser = new User
                {
                    username = username,
                    Password = password,
                    CreateDate = DateTime.UtcNow,
                };

                Context.Users.Add(NewUser);
                Context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public  bool ValidateUser(string username, string password)
        {
            User User = null;
            User = Context.Users.FirstOrDefault(Usr => Usr.username == username && Usr.Password == password);

            if (User != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public  bool DeleteUser(string username)
        {
            User User = null;
            User = Context.Users.FirstOrDefault(Usr => Usr.username == username);
            if (User != null)
            {
                Context.Users.Remove(User);
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}