﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodFellowWeb.DAO.security;
using GoodFellowWeb.Models;
using RestSharp;
using Newtonsoft.Json;
using System.Collections;
using PagedList;
using GwendNyathi.Models;

namespace GoodFellowWeb.Controllers
{
    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class UserController : Controller
    {
        public string baseUrl = "http://192.168.3.245/CMSAppG";
        familyPlanDB planDB = new familyPlanDB();
        familyPlanKinDB kinDB = new familyPlanKinDB();
        familyPlanDependentsDB depDB = new familyPlanDependentsDB();
        LoanApplicationDB loDB = new LoanApplicationDB();
        public ActionResult LoanApp()
        {
            ViewBag.DT = lonn();
            ViewBag.DT2= lonnn();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoanApp([Bind(Include = "coapplicant,First,Last,DOB,Phone,Address,OwnRent,MonthlyAmout,Email,Amount,UsedFor,Savings,JobStartDate,JobTitle,EmployerName,Hear,AnnualIncome,Tenor,Product")] LoanApplication registerreport)
        {

            if (ModelState.IsValid)
            {
                /* var client = new RestClient(baseUrl);
                 var request = new RestRequest("LoanApplication/{customernumber}/{loanamount}/{product}", Method.POST);
                 request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                 request.AddUrlSegment("customernumber",User.Identity.Name.Replace("*", "-"));
                 request.AddUrlSegment("loanamount",registerreport.LoanAmount);
                 request.AddUrlSegment("product",registerreport.product);
                 IRestResponse response = client.Execute(request);
                 string validate = response.Content;

                 validate = validate.Replace(@"""", "");
                 */string val ="Loan application submitted";
                if (registerreport.Tenor == 0)
                {
                    registerreport.Tenor = 1;
                }
                if (registerreport.Savings == null)
                {
                    registerreport.Savings =0;
                }
                loDB.Add(registerreport);
                System.Web.HttpContext.Current.Session["NOT"] = val;
                return Redirect("~/User/Loans");
            }
           ViewBag.DT2 = lonn(registerreport.Product,registerreport.Product);
            return View(registerreport);
        }
        // GET: /User/
        public ActionResult Index()
        {
           
             return View();
        }
        public ActionResult Loans(string sortOrder, string currentFilter, string Date1String, string Date2String, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.CDSSortParm = String.IsNullOrEmpty(sortOrder) ? "status" : "";
            var per = lon(User.Identity.Name);
            try
            {
                Date1String = Request.QueryString["Date1String"].ToString();
                Date2String = Request.QueryString["Date2String"].ToString();
                if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
                {
                    System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                    System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                    ViewBag.dt1 = Date1String;
                    ViewBag.dt2 = Date2String;
                }else
                {
                    System.Web.HttpContext.Current.Session["Date1"] = "";
                    System.Web.HttpContext.Current.Session["Date2"] = "";

                }
            }
            catch (Exception)
            {


            }
            try
            {
                if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                {

                    Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                    Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                    ViewBag.dt1 = Date1String;
                    ViewBag.dt2 = Date2String;
                }
            }
            catch (Exception)
            {


            }
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String) )
            {
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                DateTime date3 = date2.AddDays(1);
                per = from s in per

                      where (s.CREATED_DATE > date && s.CREATED_DATE < date3)
                      select s;

               // var cc = per.Count();
            }
            ViewBag.dt1 = Date1String;
            ViewBag.dt2 = Date2String;

            per = per.ToList();
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.ID);
                    break;
                case "status":
                    per = per.OrderByDescending(s => s.STATUS);
                    break;
                default:
                    per = per.OrderByDescending(s => s.CREATED_DATE);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            
            return View(per.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult LoansStat(string id)
        {
            ViewBag.Loan = id;
            var mc = check(User.Identity.Name,id);
            return View(mc.ToList());
        }
        public List<LoanStatement> check(string id, string ln)
        {
            List<LoanStatement> mItems=new List<LoanStatement>();
            try
            {
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Statement/{s}/{p}", Method.GET);
                request.AddUrlSegment("s",id);
                request.AddUrlSegment("p",ln);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                IRestResponse response = client.Execute(request);
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<LoanStatement> dataList = JsonConvert.DeserializeObject<List<LoanStatement>>(validate);

                var dbsel = from j in dataList

                            select j;
                decimal bals = 0;
                decimal bals22 = 0;
                decimal repay = 0;
                int count = 0;
                foreach (var c in dbsel)
                {
                    if (c.Description == "Interest to Maturity" || c.Description == "Disbursement")
                    {
                        bals22 += Convert.ToDecimal(c.Amount);
                    }

                    if (c.Description != "Interest to Maturity" || c.Description != "Disbursement")
                    {
                        repay += Convert.ToDecimal(c.Amount);
                    }
                    bals += Convert.ToDecimal(c.Amount);
                    //mItems.Add(new LoanStatement() { TrxnDate = Convert.ToDateTime(c.TrxnDate), Amount = Convert.ToDecimal(c.Amount), Refrence = c.Refrence, Description = c.Description, Balance = Convert.ToDecimal(bals) });
                    count++;
                    if (count == dbsel.Count())
                    {
                        mItems.Add(new LoanStatement() { TrxnDate = Convert.ToDateTime(c.TrxnDate), Amount = Convert.ToDecimal(bals22), Refrence = c.Refrence, Description = "Disbursed Principle + Interest to Maturity -" + (count - 2).ToString() + " Repayments", Balance = Convert.ToDecimal(bals) });

                    }
                }
                

              
            }
            catch (Exception)
            {


            }

            return mItems;
        }

        public  IEnumerable<LoanStatus> lon(string s)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("LoanStatus/{s}", Method.GET);
            request.AddUrlSegment("s",s.Replace("*", "-"));
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<LoanStatus> dataList = JsonConvert.DeserializeObject<List<LoanStatus>>(validate);

            return dataList;
        }
        public List<SelectListItem> lonn()

        {
            List<SelectListItem> phy = new List<SelectListItem>();

            var client = new RestClient(baseUrl);
            var request = new RestRequest("Products", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Products> dataList = JsonConvert.DeserializeObject<List<Products>>(validate);

            foreach (var p in dataList)
            {
                phy.Add(new SelectListItem { Text = p.DisplayName, Value = p.DisplayName });

            }


            return phy;
        }
        public List<SelectListItem> lonn(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n});

            var client = new RestClient(baseUrl);
            var request = new RestRequest("Products", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<Products> dataList = JsonConvert.DeserializeObject<List<Products>>(validate);

            foreach (var p in dataList)
            {
                phy.Add(new SelectListItem { Text = p.DisplayName, Value = p.DisplayName });

            }


            return phy;
        }
        protected override void Dispose(bool disposing)
        {
           
            base.Dispose(disposing);
        }

        public ActionResult FamilyPlan()
        {
            ViewBag.C = FamP();
            string CustNo = System.Web.HttpContext.Current.User.Identity.Name;
           ViewBag.S = CustNo;

            var trulife = planDB.ListAll(CustNo);
            int u = 0;
           
            if (trulife.Count()>0)
            {
                //get id

                FamilyPlan para_bank = new FamilyPlan();
                foreach (var z in trulife)
                {
                    para_bank.famID = z.famID;
                    para_bank.Full_Names = z.Full_Names;
                    para_bank.ID_Number_ = z.ID_Number_;
                    para_bank.Birth_Date = z.Birth_Date;
                    para_bank.Genfer = z.Genfer;
                    para_bank.PIN_Number = z.PIN_Number;
                    para_bank.Occupation = z.Occupation;
                    para_bank.Email = z.Email;
                    para_bank.Mobile_ = z.Mobile_;
                    para_bank.PBox = z.PBox;
                    para_bank.Town = z.Town;
                    para_bank.Product = z.Product;
                    para_bank.Status = z.Status;
                    para_bank.CustNo = z.CustNo;
    }
                ViewBag.S = para_bank.famID;
                return View(para_bank);

            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FamilyPlan([Bind(Include = "famID,Full_Names,ID_Number_,Birth_Date,Genfer,PIN_Number,Occupation,Email,Mobile_,PBox,Town,Product,Status")] FamilyPlan registerreport)
        {
                   
            if (ModelState.IsValid)
            {

                registerreport.Status = "Submitted";
                ViewBag.S = DateTime.Now.ToString("ddMMMMyyyyhhmmss");
                if (registerreport.famID>0)
                {
                    System.Web.HttpContext.Current.Session["NOT"] = "Family application was updated and submitted successfully.";
                    planDB.Update(registerreport);

                }
                else
                {
                System.Web.HttpContext.Current.Session["NOT"] = "Family application submitted successfully.";
                planDB.Add(registerreport);
                }
                
                ModelState.Clear();
                string CustNo = System.Web.HttpContext.Current.User.Identity.Name;
                ViewBag.S = CustNo;

                return Redirect("~/User/Index");
            }
            ViewBag.C = FamP();
            return View(registerreport);
        }
        public List<FamilyProduct> FamP()
        {
            List<FamilyProduct> lst = new List<FamilyProduct>();
            try
            {
                var client = new RestClient("http://192.168.3.245/CMSAppG/");
                var request = new RestRequest("Products2", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyProduct>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;


        }
        //plan Json

        public JsonResult ListP(string s)
        {
            var my = s;
            return Json(planDB.ListAll(s), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddP(FamilyPlan emp)
        {
            return Json(planDB.Add(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyIDP(int ID)
        {
          //  var Employee = planDB.getem().ToList();
            return Json(ID, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateP(FamilyPlan emp)
        {
            return Json(planDB.Update(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteP(int ID)
        {
            return Json(planDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }

        //Kin Json
        public JsonResult ListK(string s)
        {
            var my = s;
            return Json(kinDB.ListAll(s), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddK(FamilyPlanKIN emp)
        {
            return Json(kinDB.Add(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyIDK(int ID)
        {
            var Employee = kinDB.getem(ID).ToList();
            return Json(Employee, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateK(FamilyPlanKIN emp)
        {
            return Json(kinDB.Update(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteK(int ID)
        {
            return Json(kinDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }



        //Dependant Json
        public JsonResult ListD(string s)
        {
            var my = s;
            return Json(depDB.ListAll(s), JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddD(FamilyDependant emp)
        {
            return Json(depDB.Add(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyIDD(int ID)
        {
            var Employee = depDB.getem(ID).ToList();
            return Json(Employee, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateD(FamilyDependant emp)
        {
            return Json(depDB.Update(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteD(int ID)
        {
            return Json(depDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }
        public List<SelectListItem> lonnn()

        {
            List<SelectListItem> phy = new List<SelectListItem>();

            var client = new RestClient(baseUrl);
            var request = new RestRequest("loans", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<string> dataList = JsonConvert.DeserializeObject<List<string>>(validate);

            foreach (var p in dataList)
            {
                phy.Add(new SelectListItem { Text = p.ToString(), Value = p.ToString() });

            }


            return phy;
        }
        public List<SelectListItem> lonnn(string b)

        {
            List<SelectListItem> phy = new List<SelectListItem>();

            var client = new RestClient(baseUrl);
            var request = new RestRequest("loans", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<string> dataList = JsonConvert.DeserializeObject<List<string>>(validate);

            phy.Add(new SelectListItem { Text =b, Value =b});

            foreach (var p in dataList)
            {
                phy.Add(new SelectListItem { Text = p.ToString(), Value = p.ToString() });

            }


            return phy;
        }

    }
}
