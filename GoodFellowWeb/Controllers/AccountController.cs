﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Web.Security;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity.Migrations;
using GoodFellowWeb.DAO;
using GoodFellowWeb.Models;
using GoodFellowWeb.DAO.security;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using RestSharp;

namespace GoodFellowWeb.Controllers
{
    public class AccountController : Controller
    {
        private SBoardContext db = new SBoardContext();

        static int counter = 0;
        static object lockObj = new object();
        SBoardContext Context = new SBoardContext();
      public string  baseUrl = "http://192.168.3.245/CMSAppG";

        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model, string returnUrl = "")
        {

         
            if (ModelState.IsValid)
            {
                //model.Password = ComputeHash(model.Password, new SHA256CryptoServiceProvider());
                var user = logg(model.Username,model.Password).FirstOrDefault();
                if (user != null)
                {
                  var roles = new string[] { "User" };
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = user.UserId;
                    serializeModel.FirstName = user.MemberNumber;
                    serializeModel.LastName = user.MemberNumber;
                    serializeModel.roles = roles;
                    string role = "User";
                   string userData = JsonConvert.SerializeObject(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                            user.MemberNumber,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(60),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    System.Web.HttpCookie faCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);



                    if(roles.Contains(role))
                    {
                        counter = 0;
                       return Redirect("~/"+role+"/Index");
                    }
                 else
                    {
                        return RedirectToAction("Index", "Account");
                    }
                }
                if (counter == 0)
                {
                    var users = loggT(model.Username);
                    string name = "";
                    foreach (var p in users)
                    {
                        name = p.username;
                    }
                    if (name == model.Username)
                    {
                        lock (lockObj)
                        {
                            counter++;
                        }
                        System.Web.HttpContext.Current.Session["Status"] = model.Username;
                    }

                }
                else
                {

                    var users = loggT(model.Username);
                    string name = "";
                    foreach (var p in users)
                    {
                        name = p.username;
                    }

                    if (name == model.Username)
                    {
                        if(name == System.Web.HttpContext.Current.Session["Status"].ToString())
                        {
                        lock (lockObj)
                        {
                            counter++;
                        }
                        }
                        else
                        {
                            counter = 1;
                            System.Web.HttpContext.Current.Session["Status"] = model.Username;
                        }
                      

                    }
                }
                  
             

                var locks = logg(model.Username,model.Password);
                string name2 = "";

                 foreach(var x in locks)
                {
                    name2 = x.MemberNumber;
                }
                if (name2 == null)
                {
                    name2 = "@";
                }

                if (name2 == model.Username)
                {
                    ModelState.Clear();
                    ViewBag.Attempts = "Contact the administrator you have been locked out";
                }
                else
                {
                    if (counter > 3)
                    {
                        var users = loggL(model.Username,model.Password);
                       
                        ViewBag.Attempts = "Contact the administrator you have been locked out";
                    }
                    else
                    {
                        ModelState.Clear();
                        ViewBag.Attempts = "Number of Login attempts " + counter;
                    }

                }
                ModelState.AddModelError("", "Incorrect username and/or password");
            }
            model.Username = "";
            model.Password = "";
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Account", null);
        }

        [AllowAnonymous]
        public ActionResult StartUp() {

            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/'); ;
            return Redirect(baseUrl);
           
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        //setting forgot passsword
        public ActionResult ForgotPassword(LoginViewModel model)
        {


            return View();
        }
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(LoginViewModel model, string returnUrl = "")
        {
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Forg/{s}", Method.PUT);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s",model.Username.ToString().Replace("*", "-"));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                validate = validate.Replace(@"""", "");
                string val = validate;
            if (val!= "Password was not saved")
            {
                var mod = ModelState.First(c => c.Key == "Username");  // this
                mod.Value.Errors.Add(val);           
            }
            else
            {
                var mod = ModelState.First(c => c.Key == "Username");  // this
                mod.Value.Errors.Add("Member does not exist in our records");
            }

            return View(model);

        }
        public List<User> logg(string neme, string pass)
        {
            List<User> me = new List<User>();
            var client = new RestClient(baseUrl);
            var request = new RestRequest("Login/{s}/{p}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("s", neme.Replace(" ", ""));
            request.AddUrlSegment("p", ComputeHash(pass, new SHA256CryptoServiceProvider()));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;


            if (validate == "0")
            {
                return me;
            }
            else if (validate == "1")
            {
                var client2 = new RestClient(baseUrl);
                var request2 = new RestRequest("LoginMemA/{s}/{p}", Method.GET);
                request2.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request2.AddUrlSegment("s", neme.Replace(" ", ""));
                request2.AddUrlSegment("p", ComputeHash(pass, new SHA256CryptoServiceProvider()));
                IRestResponse response2 = client2.Execute(request2);
                var validate2 = response2.Content;
                List<User> dataList = JsonConvert.DeserializeObject<List<User>>(validate2);

                me = dataList;

            }

            return me;
        }
        public List<User> loggL(string neme, string pass)
        {
            List<User> me = new List<User>();
            var client = new RestClient(baseUrl);
            var request = new RestRequest("LoginMemL/{s}/{p}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("s", neme.Replace(" ", ""));
            request.AddUrlSegment("p", ComputeHash(pass, new SHA256CryptoServiceProvider()));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;


            if (validate == "0")
            {
                return me;
            }
            else if (validate == "1")
            {
                var client2 = new RestClient(baseUrl);
                var request2 = new RestRequest("LoginMemA/{s}/{p}", Method.GET);
                request2.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request2.AddUrlSegment("s", neme.Replace("*", "-"));
                request2.AddUrlSegment("p", ComputeHash(pass, new SHA256CryptoServiceProvider()));
                IRestResponse response2 = client2.Execute(request2);
                var validate2 = response2.Content;
                List<User> dataList = JsonConvert.DeserializeObject<List<User>>(validate);

                me = dataList;

            }

            return me;
        }
        public List<User> loggT(string neme)
        {
            List<User> me = new List<User>();
            var client2 = new RestClient(baseUrl);
            var request2 = new RestRequest("LoginMemAA/{s}", Method.GET);
            request2.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request2.AddUrlSegment("s", neme.Replace("*", "-"));
            IRestResponse response2 = client2.Execute(request2);
            var validate2 = response2.Content;
            List<User> dataList = JsonConvert.DeserializeObject<List<User>>(validate2);
            me = dataList;

            return me;
        }

    }
}