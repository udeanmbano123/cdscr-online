﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GoodFellowWeb.Controllers
{
    public class HomeController : Controller
    {
        public string baseUrl = "http://192.168.3.245/CMSAppG";
        saccoNomineeDB empDB=new saccoNomineeDB();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult WhyUS()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Team()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Services()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Login()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Register()
        {
            ViewBag.Message = "";
            ViewBag.S = DateTime.Now.ToString("ddMMMMyyyyhhmmss");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register([Bind(Include = "username,clientnumber,password,confirmpassword,surnameP,nameP,gender,title,idnumber,address,code,town,emailP,telephone,farmlocation,crops,livestock,agriculture,average,employer,workstation,designation,businessname,legalstatus")] Register registerreport)
        {
            
            if (ModelState.IsValid)
            {

                if (registerreport.gender=="Male")
                {
                    registerreport.gender = "M";
                }
                if (registerreport.gender == "Female")
                {
                    registerreport.gender = "F";
                }
                string news = Request.Form["Customer_No"].ToString();
                //registerreport.clientnumber = custno();
                registerreport.username = registerreport.clientnumber;
                var client = new RestClient("http://192.168.3.245/CMSAppG/");
                var request = new RestRequest("Register/{username/{password}/{confirmpassword}/{surname}/{name}/{gender}/{title}/{idnumber}/{address}/{code}/{town}/{email}/{telephone}/{farmlocation}/{crops}/{livestock}/{agriculture}/{average}/{employer}/{workstation}/{designation}/{businessname}/{legalstatus}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("username", registerreport.clientnumber.ToString().Replace("*", "-"));
                request.AddUrlSegment("password", ComputeHash(registerreport.password, new SHA256CryptoServiceProvider()));
                request.AddUrlSegment("confirmpassword", ComputeHash(registerreport.password, new SHA256CryptoServiceProvider()));
                request.AddUrlSegment("surname",enc(isN(registerreport.surnameP)));
                request.AddUrlSegment("name", enc(isN(registerreport.nameP)));
                request.AddUrlSegment("gender", enc(isN(registerreport.gender)));
                 request.AddUrlSegment("title", enc(isN(registerreport.title)));
                 request.AddUrlSegment("idnumber", enc(isN(registerreport.idnumber)));
                 request.AddUrlSegment("address",enc(isN(registerreport.address)));
                 request.AddUrlSegment("code", enc(isN(registerreport.code)));
                 request.AddUrlSegment("town", enc(isN(registerreport.town)));
                 request.AddUrlSegment("email", enc(isN(registerreport.emailP)));
                 request.AddUrlSegment("telephone", enc(isN(registerreport.telephone)));
                 request.AddUrlSegment("farmlocation", enc(isN(registerreport.farmlocation)));
                 request.AddUrlSegment("crops", enc(isN(registerreport.crops)));
                 request.AddUrlSegment("livestock", enc(isN(registerreport.livestock)));
                 request.AddUrlSegment("agriculture", enc(isN(registerreport.agriculture)));
                 request.AddUrlSegment("average", enc(isN(registerreport.average)));
                 request.AddUrlSegment("employer", enc(isN(registerreport.employer)));
                 request.AddUrlSegment("workstation", enc(isN(registerreport.workstation)));
                 request.AddUrlSegment("designation", enc(isN(registerreport.designation)));
                 request.AddUrlSegment("businessname", enc(isN(registerreport.businessname)));
                 request.AddUrlSegment("legalstatus", enc(isN(registerreport.legalstatus)));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                validate = validate.Replace(@"""", "");
                string val = validate;
                ViewBag.Message = "You have been registered successfully, Customer Number is " + val;

                var ne = custno(news,val);
                ModelState.Clear();
                ViewBag.S = DateTime.Now.ToString("ddMMMMyyyyhhmmss");
                return View();
            }
            ViewBag.Message = "";
                return View();
        }

        public string custno(string a,string b)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("nextNLUU/{a}/{b}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a",a);
            request.AddUrlSegment("b",b);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;


            validate = validate.Replace(@"""", "");
            string val = validate;
            return val;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if(s=="")
            {
                v = "None";
            }
           if(s == null)
            {
                v = "None";
            }

            return v;

        }
        public ActionResult Forgot()
        {
            ViewBag.Message = "";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Forgot([Bind(Include = "clientnumber")] ForgotP registerreport)
        {
            if (ModelState.IsValid)
            {
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Forg/{s}", Method.PUT);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s", registerreport.clientnumber.ToString().Replace("*", "-"));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                string val = validate;
                ViewBag.Message = val;
                ModelState.Clear();
                return View();
            }
            ViewBag.Message = "";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact([Bind(Include = "name,email,Subject,Message")] Contact registerreport)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Message = "Email was sent";
                ModelState.Clear();
                return View();
            }
            ViewBag.Message = "";
            return View();
        }
    
        public JsonResult List(string s)
        {
            var my = s;
            return Json(empDB.ListAll(s), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Add(sacco_NEXTKIN emp)
        {
            return Json(empDB.Add(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetbyID(int ID)
        {
            var Employee =empDB.getem(ID).ToList();
            return Json(Employee, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Update(sacco_NEXTKIN emp)
        {
            return Json(empDB.Update(emp), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            return Json(empDB.Delete(ID), JsonRequestBehavior.AllowGet);
        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
       
    }
}