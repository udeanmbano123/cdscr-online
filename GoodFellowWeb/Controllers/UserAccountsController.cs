﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace GoodFellowWeb.Controllers
{
    public class UserAccountsController : Controller
    {
       
        // GET: UserAccounts
        public async Task<ActionResult> Index()
        {
            return View();
        }

        // GET: UserAccounts/Details/5

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //GL_Group gL_Group = await db.GL_Groups.FindAsync(id);
            //if (gL_Group == null)
            //{
            //    return HttpNotFound();
            //}gL_Group
            return View();
        }

        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
