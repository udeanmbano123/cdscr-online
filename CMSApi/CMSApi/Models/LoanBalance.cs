﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMSApi.Models
{
    public class LoanBalance
    {
        public string Loan { get; set; }

        public double Balance { get; set; }
    }

    class LoanStatement
    {
        public DateTime TrxnDate { get; set; }
        public decimal Amount { get; set; }
        public string Refrence { get; set; }
        public string Description { get; set; }

        public decimal Balance { get; set; }
    }
}