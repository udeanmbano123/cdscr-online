﻿using CMSApi.CMS;
using CMSApi.DROID;
using CMSApi.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Http;

namespace CMSApi.Controllers
{
    public class DealsController : ApiController
    {
        CMS_ZambiaEntities db = new CMS_ZambiaEntities();
        DROID2Entities db2 = new DROID2Entities();

        // GET api/<controller>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("me")]
        [AllowAnonymous]
        public string me()
        {
            return "To go";
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoanStatus/{s}")]
        [AllowAnonymous]
        public dynamic loan(string s)
        {
            s = s.Replace("-", "*");
            var c = from p in db.QUEST_APPLICATION
                    where p.CUSTOMER_NUMBER.ToLower() == s.ToLower()
                    let AMOUNT=p.AMT_APPLIED
                    select new { p.ID,p.STATUS,AMOUNT,p.CREATED_DATE };
            return c;
        }

               [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Balances/{s}")]
        [AllowAnonymous]
        public List<LoanBalance> GetPie(string s)
        {
            s = s.Replace("-","*");
            //return listEmp.First(e => e.ID == id); 

           SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["Zambia"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select  'Loan Number '+Refrence as 'Loan', isnull(Sum(isnull(Debit,0)) - SUM(isnull(Credit,0)),0) as Balance from Accounts_Transactions a where account='" + s + "' and Refrence in (Select distinct CAST(ID As nvarchar) from Quest_Application where Customer_Number='" + s + "') group by Refrence";

            sqlCmd.Connection =myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<LoanBalance>();
            while (reader.Read())
            {
                var accountDetails = new LoanBalance
                {

                    Loan = reader.GetValue(0).ToString(),
                    Balance = Convert.ToDouble(Math.Round(Convert.ToDouble(reader.GetValue(1).ToString()),2))
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Statement/{s}/{p}")]
        [AllowAnonymous]
          public dynamic Statement(string s,string p)
        {
            s = s.Replace("-", "*");
            var c = from v in db.trans
                    where v.account_no.ToLower() == s.ToLower() && v.PolicyrefNr == p
                    let Amount = v.amount
                    let Balance = 0
                    select new { v.txn_date, Amount, v.PolicyrefNr, v.description ,Balance};

            return c;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Login/{s}/{p}")]
        [AllowAnonymous]
        public string Login(string s, string p)
        {
            s = s.Replace("-", "*");
            string res = "";
            var c = from v in db2.Users
                    where v.username == s && v.Password == p
                    select v;

            if (c.Count()>0)
            {
                res = "1";
            }else
            {
                res = "0";
            }
         
            return res;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoginMem/{s}/{p}")]
        [AllowAnonymous]
        public string LoginMem(string s, string p)
        {
            s = s.Replace("-", "*");
            string res = "";
            var c = from v in db2.Users
                    where v.username == s && v.Password == p
                    select v;
            foreach (var k in c)
            {
                res = k.MemberNumber;
            }
         
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoginMemAL/{s}/{p}")]
        [AllowAnonymous]
        public string LoginMemAL(string s, string p)
        {
            s = s.Replace("-", "*");
            
            var c = from v in db2.Users
                    where v.username == s && v.Password == p
                    select v;
            foreach (var k in c)
            {
                var my = db2.Users.Find(k.UserId);
                my.LockCount = 1;
                db2.Users.AddOrUpdate(my);
                db2.SaveChanges();
            }

            return "1";
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoginMemA/{s}/{p}")]
        [AllowAnonymous]
        public dynamic LoginMemA(string s, string p)
        {
            s = s.Replace("-", "*");
            
            var c = from v in db2.Users
                    where v.username == s && v.Password == p
                    select v;
           
            return c;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoginMemAA/{s}")]
        [AllowAnonymous]
        public dynamic LoginMemAA(string s)
        {
            s = s.Replace("-", "*");

            var c = from v in db2.Users
                    where v.MemberNumber == s
                    select v;

            return c;
        }


        [HttpPut]
        [Route("Forg/{s}")]
        [AllowAnonymous]
        public string LoginMemPutL(string s)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in db2.Users
                    where v.MemberNumber.ToLower() == s.ToLower()
                    select v;
            foreach (var k in c)
            {
                my = k.UserId;
            }
            string sms = "";
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
          
            string temp = "";
            string mobile = "";
            Random rand = new Random();
            for (int i = 0; i < 14; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            var d = db.CUSTOMER_DETAILS.ToList().Where(a=>a.CUSTOMER_NUMBER.ToLower() == s.ToLower());
            foreach (var b in d)
            {
                mobile = b.PHONE_NO;
            }
            try
            {
                User nw = db2.Users.Find(my);
                nw.Password = ComputeHash(passwordString, new SHA256CryptoServiceProvider());
                nw.ConfirmPassword = ComputeHash(passwordString, new SHA256CryptoServiceProvider());
                db2.Users.AddOrUpdate(nw);
                db2.SaveChanges();
                res = "Password was reset successfully you will receive an sms shortly";
                //send sms
                var dd = db.CUSTOMER_DETAILS.ToList().Where(a => a.CUSTOMER_NUMBER == s);
                foreach (var b in dd)
                {
                    mobile = b.PHONE_NO;
                }
                string refz = "Thank you for using our GOODFELLOW FIN mobile app your new password is " + passwordString;
                sms = SendSMS(refz, mobile);
            }
            catch (Exception)
            {

                res = "Password was not saved";
            }
            return res;
        }

        [HttpPut]
        [Route("Change/{s}/{p}")]
        [AllowAnonymous]
        public string LoginMemPut(string s, string p)
        {
            s = s.Replace("-", "*");
            string res = "";
            int my = 0;
            var c = from v in db2.Users
                    where v.username.ToLower()==s.ToLower()
                    select v;
            foreach (var k in c)
            {
                my = k.UserId;
           

            try
            {
                User nw = db2.Users.Find(my);
                nw.Password = p;
                nw.ConfirmPassword = p;
                   
                db2.Users.AddOrUpdate(nw);
              
                res = "Password was saved successfully";
            } 
          
            catch (Exception f)
            {

                res=f.Message;
            } }
            db2.SaveChanges();
            return res;
        }

        [HttpPost]
        [Route("LoanApplication/{customernumber}/{loanamount}/{product}")]
        [AllowAnonymous]
        public string LoanApplication(string customernumber,double loanamount,string product)
        {
            customernumber = customernumber.Replace("-", "*");
            string res = "";
            var c = db.CUSTOMER_DETAILS.ToList().Where(a => a.CUSTOMER_NUMBER.ToLower() == customernumber.ToLower());
            string Bank = "";
            string BankBranch = "";
            string BankAccountNo="";
            foreach (var p in c)
            {
                Bank = p.bank_name;
                BankBranch = p.bank_branch;
                BankAccountNo = p.ACCOUNT_NUMBER;
            }
            decimal? lnn = Convert.ToDecimal(loanamount);
            string prod = "";
            decimal? limit = 0;
            decimal? limit2 = 0;
            var z = db.products.ToList().Where(a => a.Product_name.ToLower().Replace(" ", "") == product.ToLower().Replace(" ", ""));
            decimal? intRate = 0;
            decimal? tenure=0;
            foreach (var x in z)
            {
                prod = x.Product_id.ToString();
                intRate=x.Interest_percentage;
              tenure=x.per_period;
                limit = x.withdrawal_limit;
                limit2 = x.withdrawal_limit;
            }

            decimal mc = Convert.ToDecimal(loanamount);
            if (mc < 0)
            {
                res = "The loan amount applied of ZMW"+mc +" is below the minimum limit of ZMW"+limit2;
            }
            else if (mc > limit)
            {
                res = "The loan amount applied of ZMW" + mc + " is above the maximum limit of ZMW" + limit;
            }
            else if(String.IsNullOrEmpty(Bank) && String.IsNullOrEmpty(BankBranch) && String.IsNullOrEmpty(BankAccountNo))
            {
                res = "The loans applied from mobile application should strictly have the client registered bank accounts details";
            }
            else
            {


                try
                {
                    foreach (var p in c)
                    {
                        QUEST_APPLICATION my = new QUEST_APPLICATION();
                        my.CUSTOMER_TYPE = p.CUSTOMER_TYPE;
                        my.SUB_INDIVIDUAL = p.SUB_INDIVIDUAL;
                        my.CUSTOMER_NUMBER = p.CUSTOMER_NUMBER;
                        my.SURNAME = p.SURNAME;
                        my.FORENAMES = p.FORENAMES;
                        my.DOB = p.DOB;
                        my.IDNO = p.IDNO;
                        my.ISSUE_DATE = p.ISSUE_DATE;
                        my.ADDRESS = p.ADDRESS;
                        my.CITY = p.CITY;
                        my.PHONE_NO = p.PHONE_NO;
                        my.NATIONALITY = p.NATIONALITY;
                        my.GENDER = p.GENDER;
                        my.HOME_TYPE = p.HOME_TYPE;
                        my.MONTHLY_RENT = p.MONTHLY_RENT;
                        my.HOME_LENGTH = p.HOME_LENGTH;
                        my.MARITAL_STATUS = p.MARITAL_STATUS;
                        my.EDUCATION = p.EDUCATION;
                        my.CURR_EMPLOYER = p.CURR_EMPLOYER;
                        my.CURR_EMP_ADD = p.CURR_EMP_ADD;
                        //my.CURR_EMP_LENGTH = p.CURR_EMP_LENGTH;
                        my.CURR_EMP_PHONE = p.CURR_EMP_PHONE;
                        my.CURR_EMP_EMAIL = p.CURR_EMP_EMAIL;
                        my.CURR_EMP_FAX = p.CURR_EMP_FAX;
                        my.CURR_EMP_CITY = p.CURR_EMP_CITY;
                        my.CURR_EMP_POSITION = p.CURR_EMP_POSITION;
                        //my.CURR_EMP_SALARY = p.CURR_EMP_SALARY;
                        //my.CURR_EMP_NET = p.CURR_EMP_NET;
                        //my.CURR_EMP_INCOME = p.CURR_EMP_INCOME;
                        my.PREV_EMPLOYER = p.PREV_EMPLOYER;
                        my.PREV_EMP_ADD = p.PREV_EMP_ADD;
                        //my.PREV_EMP_LENGTH = p.PREV_EMP_LENGTH;
                        my.PREV_EMP_PHONE = p.PREV_EMP_PHONE;
                        my.PREV_EMP_EMAIL = p.PREV_EMP_EMAIL;
                        my.PREV_EMP_FAX = p.PREV_EMP_FAX;
                        my.PREV_EMP_CITY = p.PREV_EMP_CITY;
                        my.PREV_EMP_POSITION = p.PREV_EMP_POSITION;
                        //my.PREV_EMP_SALARY = p.PREV_EMP_SALARY;
                        //my.PREV_EMP_NET = p.PREV_EMP_NET;
                        //my.PREV_EMP_INCOME = p.PREV_EMP_INCOME;
                        my.SPOUSE_NAME = p.SPOUSE_NAME;
                        my.SPOUSE_OCCUPATION = p.SPOUSE_OCCUPATION;
                        my.SPOUSE_EMPLOYER = p.SPOUSE_EMPLOYER;
                        my.SPOUSE_PHONE = p.SPOUSE_PHONE;
                        my.NO_CHILDREN = p.NO_CHILDREN;
                        my.NO_DEPENDANTS = p.NO_DEPENDANTS;
                        my.TRADE_REF1 = p.TRADE_REF1;
                        my.TRADE_REF2 = p.TRADE_REF2;
                        my.CREDIT_LIMIT = p.CREDIT_LIMIT;
                        my.HAS_ACCOUNT = p.HAS_ACCOUNT;
                        my.ACCOUNT_BRANCH = p.ACCOUNT_BRANCH;
                        my.ACCOUNT_NUMBER = p.ACCOUNT_NUMBER;
                        my.FIN_BANK = p.bank_name;
                        my.FIN_BRANCH = p.bank_branch;
                        my.FIN_BRANCH_CODE = p.bank_branch;
                        //my.BankAccountNo = p.BankAccountNo;
                        //my.BranchCode = p.BranchCode;
                        //my.BankBranch = p.BankBranch;
                        //my.FinProductType = prod;
                        //my.AMT_APPLIED = lnn;
                        my.FIN_INT_RATE = intRate;
                        my.FIN_TENOR = tenure;
                        my.CREATED_DATE = DateTime.Now;
                        my.APP_FORM_TYPE = "ANDROID";
                      my.BRANCH_NAME = "NYATHI";
                        my.BRANCH_CODE = "001";
                        my.SEND_TO = "4042";
                        my.STATUS = "Loan Application Capture";
                        my.CREATED_BY = "loan.officer";
                        my.LO_ID = "60112";
                        //my.ApprovalNumber = 1;
                        //my.ReadyToDisburse = 0;
                        //my.CUSTOMER_TYPE_ID = 2;
                        //my.RepaymentIntervalNum = 1;
                        //my.RepaymentIntervalUnit = "Months";
                        //my.FIN_REPAY_DATE = DateTime.Now.AddMonths(1);
                        my.SUB_INDIVIDUAL = p.SUB_INDIVIDUAL;
                        db.QUEST_APPLICATION.AddOrUpdate(my);
                        db.SaveChanges();
                        res = "Successful";

                        var ee = db.QUEST_APPLICATION.ToList().Where(a => a.CUSTOMER_NUMBER.ToLower() == my.CUSTOMER_NUMBER.ToLower()).Max(a => a.ID);
                        //send sms
                        var d = db.CUSTOMER_DETAILS.ToList().Where(a => a.CUSTOMER_NUMBER.ToLower() == my.CUSTOMER_NUMBER.ToLower());
                        //execute stored procedure
                        try
                        {
                            db.Database.ExecuteSqlCommand("Exec sp_amortize_balance_daily @loanID='" + ee + "'");
                        }
                        catch (Exception)
                        {

                         
                        }
                        string refz = "Thank you for using our Gwend Nyathi mobile application. You applied for a loan for ZMW" + my.AMT_APPLIED + " and Loan ID is" + ee;
                        //string sms = SendSMS(refz, my.PHONE_NO);
                    }

                }
                catch (Exception f)
                {

                    res = f.Message;
                }
            }
            return res;
        }

        [HttpPost]
        [Route("Register/{username}/{password}/{confirmpassword}/{surname}/{name}/{gender}/{title}/{idnumber}/{address}/{code}/{town}/{email}/{telephone}/{farmlocation}/{crops}/{livestock}/{agriculture}/{average}/{employer}/{workstation}/{designation}/{businessname}/{legalstatus}")]
        public dynamic Register(string username, string password, string confirmpassword,string surname,string name,string gender,string title,string idnumber,string address, string code,string town, string email, string telephone,string farmlocation, string crops, string livestock,string agriculture, string average, string employer, string workstation,string designation,string businessname,string legalstatus)
        {   //membernumber = membernumber.Replace("-", "*");
            string res = "";
           string membernumber = custno();
             surname = decodephr(surname);
             name = decodephr(name);
             gender = decodephr(gender);
             title = decodephr(title);
             idnumber = decodephr(idnumber);
            address = decodephr(address);
            code = decodephr(code);
            town = decodephr(town);
            email = decodephr(email);
            telephone = decodephr(telephone);
            farmlocation = decodephr(farmlocation);
            crops = decodephr(crops);
            livestock = decodephr(livestock);
            agriculture = decodephr(agriculture);
            average = decodephr(average);
            employer = decodephr(employer);
            workstation = decodephr(workstation);
            designation = decodephr(designation);
            businessname = decodephr(businessname);
            legalstatus= decodephr(legalstatus);
            address = address + code;
            try
            {
                
                    User my = new User();
                    my.username = membernumber;
                    my.Password = password;
                    my.ConfirmPassword = confirmpassword;
                    my.MemberNumber = membernumber;
                    my.CreateDate = DateTime.Now;
                    my.LockCount = 0;
                    db2.Users.Add(my);
                    db2.SaveChanges();
                    res =membernumber;
                    //add to customer details
                    
                    var sql2 = "delete from customer_details_back where customer_number='" + membernumber + "' insert into CUSTOMER_DETAILS_back ([CUSTOMER_TYPE],[SUB_INDIVIDUAL],[CUSTOMER_NUMBER],[SURNAME],[FORENAMES],[DOB],[IDNO],[ISSUE_DATE],[ADDRESS],[CITY],[PHONE_NO],[NATIONALITY],[GENDER],[HOME_TYPE],[MONTHLY_RENT],[HOME_LENGTH],[MARITAL_STATUS],[EDUCATION],[CURR_EMPLOYER],[CURR_EMP_ADD],[CURR_EMP_LENGTH],[CURR_EMP_PHONE],[CURR_EMP_EMAIL],[CURR_EMP_FAX],[CURR_EMP_CITY],[CURR_EMP_POSITION],[CURR_EMP_SALARY],[CURR_EMP_NET],[CURR_EMP_INCOME],[PREV_EMPLOYER],[PREV_EMP_ADD],[PREV_EMP_LENGTH],[PREV_EMP_PHONE],[PREV_EMP_EMAIL],[PREV_EMP_FAX],[PREV_EMP_CITY],[PREV_EMP_POSITION],[PREV_EMP_SALARY],[PREV_EMP_NET],[PREV_EMP_INCOME],[SPOUSE_NAME],[SPOUSE_OCCUPATION],[SPOUSE_EMPLOYER],[NO_CHILDREN],[NO_DEPENDANTS],[TRADE_REF1],[TRADE_REF2],[CREATED_BY],[CREATED_DATE],[MODIFIED_BY],[MODIFIED_DATE],[SPOUSE_PHONE],[BRANCH_CODE],[BRANCH_NAME],[AREA],[ECNO],[CD],[PAY_NUMBER], [GRADE], [POSITION], [HAS_ACCOUNT],[ACCOUNT_BRANCH],[ACCOUNT_NUMBER], SOCIETY, District, Province, Account_status, Cacu_Card, subscription, payment_ref, nok_name, nok_id, nok_address, nok_dob, nok_phone, nok_relationship, nok_email, initial_deposit, shares_value,update_type, bank_name, bank_branch, kra_pin,GroupAccount,EMAIL,Country,Region,checkoffdate) values ('Individual','"+title+"','"+membernumber+ "','"+surname+ "','" + name+ "',getdate(),'" + idnumber+ "',getdate(),'" + address+ "','" + town+ "','" + telephone+ "','Kenyan','" + gender+ "','" + farmlocation+ "',0,0,'Single','" + average+ "','" + employer+ "','" + workstation+ "','" + designation + "','','" + businessname+ "','" + legalstatus+ "','" + crops+ "','" + livestock+ "','','','','','','','','','','','','','','','','','','','','','','ONLINE',getdate(),'ONLINE',getdate(),'','','','','','','', '', '','','','','','','',0,'', '','','', '','','','','','','0','0','NEW','','','','','" + email+"','','',getdate()) delete from CUSTOMER_DETAILS_session where session_id='ONLINE'";
                    var sql = "insert into customer_details (customer_type, SUB_INDIVIDUAL, CUSTOMER_NUMBER, surname, forenames, dob, idno, ISSUE_DATE, [address], city, phone_no,nationality, gender, HOME_TYPE, MONTHLY_RENT, HOME_LENGTH, MARITAL_STATUS, EDUCATION, CURR_EMPLOYER, curr_emp_add, CURR_EMP_LENGTH, CURR_EMP_PHONE, CURR_EMP_EMAIL, curr_emp_fax, curr_emp_city, curr_emp_position, curr_emp_salary, curr_emp_net, CURR_EMP_INCOME, PREV_EMPLOYER, PREV_EMP_ADD, PREV_EMP_LENGTH, prev_emp_phone, prev_emp_email, PREV_EMP_FAX, PREV_EMP_CITY, PREV_EMP_POSITION,  PREV_EMP_SALARY, PREV_EMP_NET, prev_emp_income, SPOUSE_NAME, SPOUSE_OCCUPATION, SPOUSE_EMPLOYER, NO_CHILDREN, NO_DEPENDANTS, TRADE_REF1, TRADE_REF2, SPOUSE_PHONE, CREDIT_LIMIT, HAS_ACCOUNT, ACCOUNT_BRANCH, ACCOUNT_NUMBER, CREATED_BY,  CREATED_DATE, MODIFIED_BY, MODIFIED_DATE, BRANCH_CODE, BRANCH_NAME, AREA, deleted, ecno, cd, PAY_NUMBER, grade, POSITION, society, District, Province, Cacu_Card, subscription, payment_ref,nok_name, nok_id, nok_address, nok_dob, nok_phone, nok_relationship, nok_email, bank_name, bank_branch,GroupAccount,EMAIL,Country,Region ) select customer_type, SUB_INDIVIDUAL, CUSTOMER_NUMBER, surname, forenames, dob, idno, ISSUE_DATE, [address], city, phone_no,nationality, gender, HOME_TYPE, MONTHLY_RENT, HOME_LENGTH, MARITAL_STATUS, EDUCATION, CURR_EMPLOYER, curr_emp_add, CURR_EMP_LENGTH, CURR_EMP_PHONE, CURR_EMP_EMAIL, curr_emp_fax, curr_emp_city, curr_emp_position, curr_emp_salary, curr_emp_net, CURR_EMP_INCOME, PREV_EMPLOYER, PREV_EMP_ADD, PREV_EMP_LENGTH, prev_emp_phone, prev_emp_email, PREV_EMP_FAX, PREV_EMP_CITY, PREV_EMP_POSITION,  PREV_EMP_SALARY, PREV_EMP_NET, prev_emp_income, SPOUSE_NAME, SPOUSE_OCCUPATION, SPOUSE_EMPLOYER, NO_CHILDREN, NO_DEPENDANTS, TRADE_REF1, TRADE_REF2, SPOUSE_PHONE, CREDIT_LIMIT, HAS_ACCOUNT, ACCOUNT_BRANCH, ACCOUNT_NUMBER, CREATED_BY,  CREATED_DATE, MODIFIED_BY, MODIFIED_DATE, BRANCH_CODE, BRANCH_NAME, AREA, deleted, ecno, cd, PAY_NUMBER, grade, POSITION, society, District, Province, Cacu_Card, subscription, payment_ref,nok_name, nok_id, nok_address, nok_dob, nok_phone, nok_relationship, nok_email, bank_name, bank_branch,GroupAccount,EMAIL,RegistrationNumber,InstitutionName,CmpPostalAddress,CmpPhysicalAddress,CmpPaymentRef,GrpDate,ContactPerson,ContactEmail,ContactTel,ContactPosition,ContactReference,Country,Region,kra_pin,checkoffdate from CUSTOMER_DETAILS_back where CUSTOMER_NUMBER='" + membernumber + "' update customer_details_back set Account_status='2',Approver1='ONLINE' where customer_number='" + membernumber + "'";
                    int mey = db.Database.ExecuteSqlCommand(sql2);
                  //int mee = db.Database.ExecuteSqlCommand(sql);
                

            }
            catch (Exception z)
            {
                res = z.Message;
                res =z.Message;
            }

            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Member")]
        [AllowAnonymous]
        public dynamic prodsd()
        {
            string res = "";
            var mwa = db.Database.SqlQuery<CRV>("SELECT (select [prefix] from para_account_no)++RIGHT('00000000000000000000000000000000000000'+ CONVERT(VARCHAR,custno+1),(select [length] from para_account_no))+(select [suffix] from para_account_no) AS custno from maxid where custno is not null");
            foreach(var p  in mwa){
                res = p.custno;
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Products2")]
        [AllowAnonymous]
        public dynamic we()
        {

            var mwa = db.FamilyProducts.ToList();
        
            return mwa;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamilyPlan/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}")]
        [AllowAnonymous]
        public dynamic prTD(string a, string b, string c, string d, string e, string f, string g, string h, string i,string j, string k,string l)
        {
            string res = "Successfully added";

            try
            {
                if (c==null)
                {
                    c = DateTime.Now.ToString("dd-MMM-yyyy");
                }
                if (c == "None")
                {
                    c = DateTime.Now.ToString("dd-MMM-yyyy");
                }
                FamilyPlan my = new FamilyPlan();
                my.Full_Names = decodephr(a);
                my.ID_Number_ = decodephr(b);
                try
                {
                    my.Birth_Date = Convert.ToDateTime(decodephr(c));
                }
                catch (Exception)
                {

                    my.Birth_Date = Convert.ToDateTime(c);
                }
                my.Genfer = decodephr(d);
                my.PIN_Number = decodephr(e);
                my.Occupation = decodephr(f);
                my.Email = decodephr(g);
                my.Mobile_ = decodephr(h);
                my.PBox = decodephr(i);
                my.Town = decodephr(j);
                my.Product = decodephr(k);
                my.CustNo = decodephr(l);
                my.Status = "Submitted";
                db.FamilyPlans.Add(my);
                db.SaveChanges();


                try
                {
                    var remove = db.FamilyPlans.ToList().Where(z => z.CustNo == my.CustNo).FirstOrDefault();
                    //int count = db.Accounts_Documentss.ToList().Where(a => string.IsNullOrEmpty(a.doc_generated)).Count();
                    int myy = db.Database.ExecuteSqlCommand("Update FamilyPlanKIN set PlanNo='" + remove.famID.ToString() + "' where PlanNo='" + my.CustNo + "'");
                    int myyy = db.Database.ExecuteSqlCommand("Update FamilyDependants set PlanNo='" + remove.famID.ToString() + "' where PlanNo='" + my.CustNo + "'");

                }
                catch (Exception)
                {

                }
            }
            catch (Exception z)
            {

                res = z.Message;
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamilyPlanU/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}/{m}")]
        [AllowAnonymous]
        public dynamic prTDz(string a, string b, string c, string d, string e, string f, string g, string h, string i, string j, string k,string l,string m)
        {
            
            string res = "Successfully updated";
            try
            {
                int ne = Convert.ToInt32(l);
                var my = db.FamilyPlans.Find(ne);
                my.Full_Names = decodephr(a);
                my.ID_Number_ = decodephr(b);
                my.Birth_Date = Convert.ToDateTime(decodephr(c));
                my.Genfer = decodephr(d);
                my.PIN_Number = decodephr(e);
                my.Occupation = decodephr(f);
                my.Email = decodephr(g);
                my.Mobile_ = decodephr(h);
                my.PBox = decodephr(i);
                my.Town = decodephr(j);
                my.Product = decodephr(k);
                my.CustNo = decodephr(m);
                my.Status = "Submitted";
                db.FamilyPlans.AddOrUpdate(my);
                db.SaveChanges();
            }
            catch (Exception z)
            {

                res = z.Message+" "+ decodephr(l) + " " + decodephr(c);
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanR/{a}")]
        [AllowAnonymous]
        public dynamic prTD(string a)
        {
            string res = "Successfully removed";
      
            try
            {
                int mwe = Convert.ToInt32(a);
                FamilyPlan my = db.FamilyPlans.Find(mwe);
                db.FamilyPlans.Remove(my);
                db.SaveChanges();
            }
            catch (Exception d)
            {

                res = d.Message;
            }


            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanKIN/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}")]
        [AllowAnonymous]
        public dynamic prTT(string a, string b, string c,string d,string e,string f,string g,string h)
        {
            string res = "Successfully added";
            try
            {
                
                FamilyPlanKIN my = new FamilyPlanKIN();
                my.Name = decodephr(a);
                my.ID_Number = decodephr(b);
                try
                {
                    my.DOB = Convert.ToDateTime(decodephr(c).Replace(" ",""));
                }
                catch (Exception)
                {

                   // my.DOB = null;
                }
                my.Relationship = decodephr(d);
                my.Email = decodephr(e);
                my.Phone = decodephr(f);
                my.Address = decodephr(g);
                my.PlanNo = decodephr(h);
                db.FamilyPlanKINs.Add(my);
                db.SaveChanges();
            }
            catch (Exception z)
            {
                res = z.Message + ","+ decodephr(c);
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanKINU/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}")]
        [AllowAnonymous]
        public dynamic prTK(string a, string b, string c, string d, string e, string f, string g, string h,string i)
        {
            string res = "Successfully updated";
            int sl = Convert.ToInt32(i);
            try
            {
                if (c == null)
                {
                    c = DateTime.Now.ToString("dd-MMM-yyyy");
                }
                if (c == "None")
                {
                    c = DateTime.Now.ToString("dd-MMM-yyyy");
                }
                var my = db.FamilyPlanKINs.Find(i);
                my.Name = decodephr(a);
                my.ID_Number = decodephr(b);
                try
                {
                    my.DOB = Convert.ToDateTime(decodephr(c));
                }
                catch (Exception)
                {

                    //my.DOB =null;
                }
                my.Relationship = decodephr(d);
                my.Email = decodephr(e);
                my.Phone = decodephr(f);
                my.Address = decodephr(g);
                my.PlanNo = decodephr(h);
                db.FamilyPlanKINs.Add(my);
                db.SaveChanges();
            }
            catch (Exception z)
            {
                res = z.Message;
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanKIND/{a}")]
        [AllowAnonymous]
        public dynamic prTSSD(string a)
        {
            string res = "Successfully removed";

            try
            {
                int mwe = Convert.ToInt32(a);
                FamilyPlanKIN my = db.FamilyPlanKINs.Find(mwe);
                db.FamilyPlanKINs.Remove(my);
                db.SaveChanges();
            }
            catch (Exception d)
            {

                res = d.Message;
            }


            return res;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanKINDD/{a}")]
        [AllowAnonymous]
        public dynamic prTSSDD(string a)
        {
            var mwa = db.FamilyPlanKINs.ToList().Where(z=>z.PlanNo==a);


            return mwa;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanKINDDD/{a}")]
        [AllowAnonymous]
        public dynamic prTSSDDD(string a)
        {
            int w = Convert.ToInt32(a);
            var mwa = db.FamilyPlanKINs.ToList().Where(z => z.id == w);


            return mwa;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanD/{a}/{b}/{c}/{d}/{e}/{f}")]
        [AllowAnonymous]
        public dynamic prT(string a,string b,string c,string d,string e,string f)
        {
            string res = "Successfully added";
            try
            {
                FamilyDependant my = new FamilyDependant();
                my.FullNames = decodephr(a);
                my.Relationship = decodephr(b);
                my.Email = decodephr(c);
                my.Phone = decodephr(d);
                my.PlanNo = decodephr(e);
                my.DOB = Convert.ToDateTime(decodephr(f));
                db.FamilyDependants.Add(my);
                db.SaveChanges();
            }
            catch (Exception z)
            {

                res = z.Message;
            }

            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanDU/{a}/{b}/{c}/{d}/{e}/{f}/{g}")]
        [AllowAnonymous]
        public dynamic prTK(string a, string b, string c, string d, string e,string f,string g)
        {
            int i = Convert.ToInt32(g);

            string res = "Successfully added";
            try
            {
                var my = db.FamilyDependants.Find(i);
                my.FullNames = decodephr(a);
                my.Relationship = decodephr(b);
                my.Email = decodephr(c);
                my.Phone = decodephr(d);
                my.PlanNo = decodephr(e);
                my.DOB = Convert.ToDateTime(decodephr(f));
                db.FamilyDependants.AddOrUpdate(my);
                db.SaveChanges();
            }
            catch (Exception z)
            {

                res = z.Message;
            }

            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanD/{a}")]
        [AllowAnonymous]
        public dynamic prTDDD(string a)
        {

            string res = "Successfully removed";

            try
            {
                int mwe = Convert.ToInt32(a);
                FamilyDependant my = db.FamilyDependants.Find(mwe);
                db.FamilyDependants.Remove(my);
                db.SaveChanges();
            }
            catch (Exception d)
            {

                res = d.Message;
            }


            return res;

           
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanDD/{a}")]
        [AllowAnonymous]
        public dynamic prTDDWD(string a)
        {

            int ne = Convert.ToInt32(a);
            var se = db.FamilyDependants.ToList().Where(z => z.id == ne);
            return se;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlanDDD/{a}")]
        [AllowAnonymous]
        public dynamic prTDDDWD(string a)
        {

            //int ne = Convert.ToInt32(a);
            var se = db.FamilyDependants.ToList().Where(z => z.PlanNo == a);
            return se;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("FamiyPlans/{a}")]
        [AllowAnonymous]
        public dynamic prTDDWDD(string a)
        {

            string res = "Successfully removed";
            var se = db.FamilyPlans.ToList().Where(z=>z.CustNo==a);
            return se;
            
        }
        public string custno()
        {
            string res = "";
        var mwa = db.Database.SqlQuery<CRV>("SELECT (select [prefix] from para_account_no)++RIGHT('00000000000000000000000000000000000000'+ CONVERT(VARCHAR,custno+1),(select [length] from para_account_no))+(select [suffix] from para_account_no) AS custno from maxid where custno is not null");
            foreach(var p  in mwa){
                res = p.custno;
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Products")]
        [AllowAnonymous]
        public dynamic prods()
        {
            var c = from p in db.products
                    
                    select new { p.product_code, p.Product_name};
            return c;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoanN/{t}")]
        [AllowAnonymous]
        public dynamic chok(string t)
        {
            t = t.Replace("-", "*");
          
            var sf = db.Database.SqlQuery<LCounts>("select status,count(id) as numLoans from QUEST_APPLICATION where Customer_Number='"+ t +"' group by status");
           
            return sf;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("nameN/{t}")]
        [AllowAnonymous]
        public dynamic chokN(string t)
        {
            t = t.Replace("-", "*");

            var sf = db.CUSTOMER_DETAILS_back.ToList().Where(a=>a.CUSTOMER_NUMBER==t).Select(a=>a.SURNAME+" "+a.FORENAMES);
                  
                     return sf;

                }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("nextN/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}")]
        [AllowAnonymous]
        public dynamic nxtN(string a,string b,string c,string d,string e,string f,string g,string h,string i,string j,string k)
        {
            string res = "";
            try
            {
               Sacco_next_of_kin my = new Sacco_next_of_kin();
                my.Surname = decodephr(a);
                my.Name = decodephr(b);
                my.OtherNames = decodephr(c);
                my.PBox = decodephr(d);
                my.PostalCode = decodephr(e);
                my.ID_Number = decodephr(f);
                my.Email = decodephr(g);
                my.Phone = decodephr(h);
                my.City = decodephr(i);
                my.CustomerNo = decodephr(j);
                my.Percentage = Convert.ToDecimal(decodephr(k));
                db.Sacco_next_of_kin.Add(my);
                db.SaveChanges();
                res = "Successfully updated";
            }
            catch (Exception r)
            {

                res = r.Message;
            }
             return res;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST","PUT")]
        [System.Web.Http.HttpPut]
        [Route("nextNUpdate/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}")]
        [AllowAnonymous]
        public dynamic nxtN(string a, string b, string c, string d, string e, string f, string g, string h, string i, string j,string k,string l)
        {
            string res = "";
            int mwe = Convert.ToInt32(l);
            try
            {
                var my = db.Sacco_next_of_kin.Find(mwe);
                my.Surname = decodephr(a);
                my.Name = decodephr(b);
                my.OtherNames = decodephr(c);
                my.PBox = decodephr(d);
                my.PostalCode = decodephr(e);
                my.ID_Number = decodephr(f);
                my.Email = decodephr(g);
                my.Phone = decodephr(h);
                my.City = decodephr(i);
                my.CustomerNo = decodephr(j);
                my.Percentage = Convert.ToDecimal(decodephr(k));
                db.Sacco_next_of_kin.AddOrUpdate(my);
                db.SaveChanges();
                res = "Successfully updated";
            }
            catch (Exception r)
            {

                res = r.Message;
            }
            return res;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST","DELETE")]
        [System.Web.Http.HttpDelete]
        [Route("nextNR/{a}")]
        [AllowAnonymous]
        public dynamic nxtNR(string a)
        {
            string res = "Successfully removed";

            try
            {
                int mwe = Convert.ToInt32(a);
                Sacco_next_of_kin my = db.Sacco_next_of_kin.Find(mwe);
                db.Sacco_next_of_kin.Remove(my);
                db.SaveChanges();
            }
            catch (Exception d)
            {

                res = d.Message;
            }
            return res;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("nextNL/{a}")]
        [AllowAnonymous]
        public dynamic nxtNL(string a)
        {
            var my = db.Sacco_next_of_kin.ToList().Where(z=>z.CustomerNo==a);
            return my;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("nextNLU/{a}")]
        [AllowAnonymous]
        public dynamic nxtNLU(string a)
        {
            int mwe = Convert.ToInt32(a);
            var my = db.Sacco_next_of_kin.ToList().Where(z => z.id == mwe);
            return my;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("nextNLUU/{a}/{b}")]
        [AllowAnonymous]
        public dynamic nxtNLUU(string a,string b)
        {
            var my = "Successfully updated";
            var remove = db.Sacco_next_of_kin.ToList().Where(z=>z.CustomerNo==a);
            //int count = db.Accounts_Documentss.ToList().Where(a => string.IsNullOrEmpty(a.doc_generated)).Count();
            foreach (var detail in remove)
            {
                Sacco_next_of_kin user = db.Sacco_next_of_kin.Find(detail);
                user.CustomerNo = b;
                db.Sacco_next_of_kin.AddOrUpdate(user);
                db.SaveChanges();
            }
            try
            {

            }
            catch (Exception f)
            {

                my = f.Message;
            }
            return my;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("LoApp/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}/{m}/{n}/{o}/{p}/{q}/{r}/{s}/{t}")]
        [AllowAnonymous]
        public dynamic chokNNQC(string a, string b, string c, string d, string e, string f, string g, string h, string i, string j, string k, string l,string m,string n,string o,string p,string q,string r,string s,string t)
        {
            string res="Successfully applied";
            string chk = "";
            var sf = db.CUSTOMER_DETAILS_back.ToList().Where(z => z.CUSTOMER_NUMBER == decodephr(r)).FirstOrDefault();
            try
            {
                DateTime mwa = DateTime.Now;
                /*QUEST_APPLICATION my = new QUEST_APPLICATION();
                my.CUSTOMER_TYPE = "Individual";
                //cooperation
                my.SUB_INDIVIDUAL = decodephr(a);
                my.CUSTOMER_NUMBER = decodephr(r);
                my.SURNAME = decodephr(c);
                my.FORENAMES = decodephr(b);
                //my.DOB = Convert.ToDateTime(decodephr(d));
                my.IDNO = sf.IDNO;
                my.ISSUE_DATE = mwa;
                my.ADDRESS = decodephr(f);
                my.CITY = sf.CITY;
                my.PHONE_NO = decodephr(e);
                my.NATIONALITY = sf.NATIONALITY;
                my.GENDER = sf.GENDER;
                my.HOME_TYPE = "0";
                my.MONTHLY_RENT = Convert.ToDecimal(decodephr(h));
                my.MARITAL_STATUS = sf.MARITAL_STATUS;
                my.EDUCATION = sf.EDUCATION;
                my.CURR_EMPLOYER = decodephr(o);
                my.CURR_EMP_POSITION = decodephr(n);
                my.CURR_EMP_NET = Convert.ToDecimal(0.00);
                my.CURR_EMP_INCOME = Convert.ToDecimal(decodephr(q));
                my.HAS_ACCOUNT = true;
                my.ACCOUNT_NUMBER = sf.ACCOUNT_NUMBER;
                my.FIN_AMT = Convert.ToDecimal(decodephr(j));
                my.FIN_TENOR = Convert.ToDecimal(6);
                my.FIN_INT_RATE =Convert.ToDecimal(1.00);
                my.FIN_PURPOSE = decodephr(k);
               my.FIN_SEC_OFFER = decodephr(j);
                my.FIN_BANK = sf.bank_name;
                my.FIN_BRANCH = sf.BRANCH_CODE;
                my.FIN_BRANCH_CODE = sf.bank_branch;
                my.FIN_ACCNO = sf.ACCOUNT_NUMBER;
                my.FIN_REPAY_DATE = my.ISSUE_DATE;
               my.OTHER_AMT = 1;
                my.QUES_HOW = decodephr(g);
                my.APP1_APPROVED = false;
                my.RECOMMENDED_AMT = Convert.ToDecimal(decodephr(j));
                //my.REC_DATE = Convert.ToDateTime(decodephr(m));
                my.RECOMMENDED = false;
                my.DISBURSED = false;
                my.APP2_APPROVED = false;
                my.INSTALLMENT = my.FIN_AMT / my.FIN_TENOR;
                my.STATUS = "SUBMITTED";
                my.SEND_TO = "4043";
                my.CREATED_BY = "ONLINE";
                my.MODIFIED_BY = "ONLINE";
                my.REPAID = false;
                my.AMT_APPLIED = my.FIN_AMT;
                my.ECNO = sf.ECNO;
                my.APPROVED_FOR_DISBURSAL = false;
                my.loan_type = "ORDLOAN";
                my.Payout = my.FIN_AMT;
                my.Reference = "N";
                db.QUEST_APPLICATION.Add(my);
 db.SaveChanges();*/
                var dafq = "insert into QUEST_APPLICATION ([CUSTOMER_TYPE],[SUB_INDIVIDUAL],[CUSTOMER_NUMBER],[SURNAME],[FORENAMES],[DOB],[IDNO],[ISSUE_DATE],[ADDRESS],[CITY],[PHONE_NO],[NATIONALITY],[GENDER],[HOME_TYPE],[MONTHLY_RENT],[HOME_LENGTH],[MARITAL_STATUS],[EDUCATION],[CURR_EMPLOYER],[CURR_EMP_ADD],[CURR_EMP_LENGTH],[CURR_EMP_PHONE],[CURR_EMP_EMAIL],[CURR_EMP_FAX],[CURR_EMP_CITY],[CURR_EMP_POSITION],[CURR_EMP_SALARY],[CURR_EMP_NET],[CURR_EMP_INCOME],[PREV_EMPLOYER],[PREV_EMP_ADD],[PREV_EMP_LENGTH],[PREV_EMP_PHONE],[PREV_EMP_EMAIL],[PREV_EMP_FAX],[PREV_EMP_CITY],[PREV_EMP_POSITION],[PREV_EMP_SALARY],[PREV_EMP_NET],[PREV_EMP_INCOME],[SPOUSE_NAME],[SPOUSE_OCCUPATION],[SPOUSE_EMPLOYER],[NO_CHILDREN],[NO_DEPENDANTS],[TRADE_REF1],[TRADE_REF2],[GUARANTOR_NAME],[GUARANTOR_DOB],[GUARANTOR_IDNO],[GUARANTOR_PHONE],[GUARANTOR_ADD],[GUARANTOR_CITY],[GUARANTOR_HOME_TYPE],[GUARANTOR_RENT],[GUARANTOR_HOME_LENGTH],[GUARANTOR_EMPLOYER],[GUARANTOR_EMP_ADD],[GUARANTOR_EMP_LENGTH],[GUARANTOR_EMP_PHONE],[GUARANTOR_EMP_EMAIL],[GUARANTOR_EMP_FAX],[GUARANTOR_EMP_POSTN],[GUARANTOR_EMP_SALARY],[GUARANTOR_EMP_INCOME],[GUARANTOR_REL_NAME],[GUARANTOR_REL_ADD],[GUARANTOR_REL_CITY],[GUARANTOR_REL_PHONE],[GUARANTOR_REL_RELTNSHP],[FIN_AMT],[FIN_TENOR],[FIN_INT_RATE],[FIN_PURPOSE],[FIN_SRC_REPAYMT],[FIN_SEC_OFFER],[FIN_BANK],[FIN_BRANCH],[FIN_BRANCH_CODE],[FIN_ACCNO],[FIN_REPAY_DATE],[OTHER_DESC],[OTHER_ACCNO],[OTHER_AMT],[QUES_HOW],[QUES_EMPLOYEE],[QUES_AGENT],[APPL_SIGNATURE],[APPL_DATE],[JOINT_APPL_SIGNATURE],[APP1_APPROVED],[RECOMMENDED_AMT],[APP1_SIGNATURE],[APP2_APPROVED],[APPROVED_AMT],[APP2_SIGNATURE],[INSTALLMENT],[PERIOD],[CREATED_BY],[CREATED_DATE],[MODIFIED_BY],[MODIFIED_DATE],[STATUS],[SEND_TO],[BRANCH_CODE],[BRANCH_NAME],[AREA],[AMT_APPLIED],[ECOCASH_NUMBER],[OTHER_CHARGES],[DISBURSE_OPTION],LO_ID,LAST_ID, loan_type,payout, prev_balance,[Reference]) values ('Individual','" + decodephr(a) + "','" + decodephr(r) + "','" + decodephr(c) + "','" + decodephr(b) + "',CAST('" + decodephr(d) + "' AS datetime),'0',getdate(),'" + decodephr(f) + "','','" + decodephr(e) + "','0','0','0','" + decodephr(h) + "','0','M','0','" + decodephr(o) + "','0','0','0','0','0','0','" + decodephr(n) + "','" + decodephr(q) + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0',getdate(),'0','0','0','0','0','0','0','0','0','0','0','" + decodephr(i) + "','0','0','0','0','0','0','0','0','0','" + decodephr(j) + "','"+ decodephr(s) + "',(select Interest_Percentage from para_loan_instr where Loan_Type='"+ decodephr(t) + "'),'" + decodephr(k) + "','" + decodephr(q) + "','" + decodephr(l) + "','0','0','0','0',DATEADD(month,1,getdate()),'0','0','0','0','0','0','','','','','','','','','','" + Convert.ToDecimal(decodephr(j))/Convert.ToDecimal(decodephr(s)) + "','','ONLINE',getdate(),'','','SUBMITTED','4043','0','0','0','" + decodephr(j) + "','0','0','BANK','ONLINE','ONLINE',(select loan_code from para_loan_instr where Loan_Type='" + decodephr(t) + "'),'0','0','N')";

               chk = decodephr(a)+","+ decodephr(b) + "," + decodephr(c) + "," + decodephr(d) + "," + decodephr(e) + "," + decodephr(f) + "," + decodephr(g) + "," + decodephr(h) + "," + decodephr(i) + "," + decodephr(j) + "," + decodephr(k) + "," + decodephr(l) + "," + decodephr(m) + "," + decodephr(n) + "," + decodephr(o) + "," + decodephr(p) + "," + decodephr(q) + "," + decodephr(r);
                int se = db.Database.ExecuteSqlCommand(dafq);



                /*
             my.CUSTOMER_TYPE = "Individual";
            my.SUB_INDIVIDUAL = decodephr(a);
             my.CUSTOMER_NUMBER = decodephr(r);
             my.SURNAME = decodephr(b);
             my.FORENAMES = decodephr(a);
             my.DOB = Convert.ToDateTime(decodephr(d));
             my.IDNO = sf.IDNO;
             my.ISSUE_DATE = DateTime.Now;
             my.ADDRESS = decodephr(f);
             my.CITY = sf.CITY;
             my.PHONE_NO = decodephr(e);
             my.NATIONALITY = sf.NATIONALITY;
             my.GENDER = sf.GENDER;
             my.HOME_TYPE = "0";
             my.MONTHLY_RENT = Convert.ToDecimal(decodephr(h));
             my.HOME_LENGTH = 0;
             my.MARITAL_STATUS = sf.MARITAL_STATUS;
             my.EDUCATION = sf.EDUCATION;
             my.CURR_EMPLOYER = decodephr(o);

             my.CURR_EMP_LENGTH = 0;
             my.CURR_EMP_POSITION = decodephr(n);
             my.CURR_EMP_SALARY = 0;

                 my.CURR_EMP_NET = Convert.ToDecimal(sf.CURR_EMP_NET);
             my.CURR_EMP_INCOME = Convert.ToDecimal(q);
             my.PREV_EMP_LENGTH = 0;
             my.PREV_EMP_SALARY = 0;
             my.PREV_EMP_NET = 0;
             my.PREV_EMP_INCOME = 0;
             my.NO_CHILDREN = 0;
             my.NO_DEPENDANTS = 0;
             my.CREDIT_LIMIT = 0;
             my.HAS_ACCOUNT = true;
             my.ACCOUNT_NUMBER = sf.ACCOUNT_NUMBER;

             my.FIN_AMT = Convert.ToDecimal(decodephr(j));
             my.FIN_TENOR = 6;
             my.FIN_INT_RATE = 1;
             my.FIN_ADMIN = 0;
             my.FIN_PURPOSE = decodephr(k);
            my.FIN_SEC_OFFER = decodephr(j);
             my.FIN_BANK = sf.bank_name;
             my.FIN_BRANCH = sf.BRANCH_CODE;
             my.FIN_BRANCH_CODE = sf.bank_branch;
             my.FIN_ACCNO = sf.ACCOUNT_NUMBER;
             my.FIN_REPAY_DATE = my.ISSUE_DATE;
            my.OTHER_AMT = 1;
             my.QUES_HOW = decodephr(g);
            my.APPL_DATE = my.ISSUE_DATE;
             my.APP1_APPROVED = false;
             my.RECOMMENDED_AMT = Convert.ToDecimal(decodephr(j));
             my.REC_DATE = Convert.ToDateTime(decodephr(m));
             my.RECOMMENDED = false;
             my.DISBURSED = false;
             my.DISBURSED_DATE = null;
             my.APP1_DATE = my.ISSUE_DATE;
             my.APP2_APPROVED = false;
             my.APPROVED_AMT = 0;
             my.APP2_SIGNATURE = null;
             my.APP2_DATE = my.ISSUE_DATE;
             my.INSTALLMENT = my.FIN_AMT / my.FIN_TENOR;
             my.STATUS = "SUBMITTED";
             my.SEND_TO = "4043";
             my.CREATED_BY = "ONLINE";
             my.CREATED_DATE = my.ISSUE_DATE;
             my.MODIFIED_BY = "ONLINE";
             my.MODIFIED_DATE = my.ISSUE_DATE;
             my.REPAID = false;
             my.AMT_APPLIED = my.FIN_AMT;
             my.OTHER_CHARGES = 0;
             my.ECNO = sf.ECNO;
             my.SSB_MONTHLY_RATE = 0;
             my.APPROVED_FOR_DISBURSAL = false;
             my.loan_type = "ORDLOAN";
             my.input_quantity = 0;
             my.Payout = my.FIN_AMT;
             my.Prev_balance = 0;
             my.Reference = "N";
            */
            }
            catch (Exception z)
            {

                res = z.Message +" "+ chk;
            }

           

            return res;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("loans")]
        [AllowAnonymous]
        public dynamic loansa()
        {
          
            var my = db.para_loan_instr.ToList().Select(a=>a.Loan_Type).Distinct();
            return my;
        }

        public string decodephr(string s)
        {
            string str = "";
            try
            {
                s = s.Replace('-', '+');
                s=s.Replace('_', '/');
                byte[] bytes = Convert.FromBase64String(s);
                str = Encoding.UTF8.GetString(bytes);
            }
            catch (Exception)
            {

                str = "0";
            }
            return str;
        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }


        private string SendSMS(string strMsg, string MobNo)
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                string url = "http://www.savannacom.zm/smsservices/bms_dot_php_third_party_api.php?customerid=149&from=gfdl&to=" + MobNo + "&message=" + strMsg + "";
                //var client2 = new RestClient("http://etext.co.zw/sendsms.php?user=263773360785&password=simbaj80&mobile=" + MobNo + "&senderid=OLDMUT&message=" + strMsg + "");
                var client2 = new RestClient(url);
                var request2 = new RestRequest("", Method.GET);
                IRestResponse response2 = client2.Execute(request2);
                string validate2 = response2.Content;
                SmsStatusMsg=validate2;
            }
            catch (Exception f)
            {

                SmsStatusMsg = f.Message;
            }
            return SmsStatusMsg;

        }

        public void SendMail(string id, string refz, string name)
        {
            //SendMail(email,refz,HttpUtility.UrlDecode(name));
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mm = new MailMessage("edocwebapp@gmail.com", id, "Credit Management App " + name, refz);
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);

        }


    }
}
 